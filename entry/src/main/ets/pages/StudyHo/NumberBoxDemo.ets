// NumberBoxDemo.ets
// NumberBox步进器使用示例

import { NumberBox } from '../../components/NumberBox';

@Entry
@Component
struct NumberBoxDemo {
    @State value1: number = 5;  // 基础用法
    @State value2: number = 3;  // 步长设置
    @State value3: number = 5;  // 限制输入范围
    @State value4: number = 3;  // 限制输入整数
    @State value5: number = 3;  // 禁用状态
    @State value6: number = 3;  // 禁用输入框
    @State value7: number = 3;  // 禁用长按
    @State value8: number = 3.1;  // 固定小数位数
    @State value9: number = 3;  // 异步变更
    @State value10: number = 3;  // 自定义小颜色样式
    @State value11: number = 3;  // 自定义加减按钮图标

    build() {
        Column() {
            // 标题
            Text('NumberBox 步进器组件示例')
                .fontSize(20)
                .fontWeight(FontWeight.Bold)
                .margin({ bottom: 20 })

            // 基础用法
            Row() {
                Text('基础用法')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value1,
                    onChange: (value: number) => {
                        this.value1 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 步长设置
            Row() {
                Text('步长设置')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value2,
                    step: 2,  // 设置步长为2
                    onChange: (value: number) => {
                        this.value2 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 限制输入范围
            Row() {
                Text('限制输入范围')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value3,
                    min: 2,  // 最小值为2
                    max: 8,  // 最大值为8
                    onChange: (value: number) => {
                        this.value3 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 限制输入整数
            Row() {
                Text('限制输入整数')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value4,
                    decimalLength: 0,  // 设置小数位数为0，即只能输入整数
                    onChange: (value: number) => {
                        this.value4 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 禁用状态
            Row() {
                Text('禁用状态')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value5,
                    disabled: true,  // 禁用整个组件
                    onChange: (value: number) => {
                        this.value5 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 禁用输入框
            Row() {
                Text('禁用输入框')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value6,
                    disableInput: true,  // 禁用输入框，只能通过按钮改变值
                    onChange: (value: number) => {
                        this.value6 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 禁用长按
            Row() {
                Text('禁用长按')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value7,
                    disableLongPress: true,  // 禁用长按功能
                    onChange: (value: number) => {
                        this.value7 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 固定小数位数
            Row() {
                Text('固定小数位数')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value8,
                    decimalLength: 1,  // 设置小数位数为1
                    onChange: (value: number) => {
                        this.value8 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 异步变更
            Row() {
                Text('异步变更')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value9,
                    onChange: (value: number) => {
                        // 模拟异步操作，延迟更新值
                        setTimeout(() => {
                            this.value9 = value;
                        }, 500);
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 自定义小颜色样式
            Row() {
                Text('自定义小颜色样式')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value10,
                    buttonColor: '#007aff',  // 自定义按钮颜色
                    iconColor: '#ffffff',    // 自定义图标颜色
                    onChange: (value: number) => {
                        this.value10 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)

            // 自定义加减按钮图标
            Row() {
                Text('自定义加减按钮图标')
                    .width('40%')
                    .fontSize(16)
                NumberBox({
                    value: this.value11,
                    minusIcon: '/common/minus.png',  // 自定义减少按钮图标
                    plusIcon: '/common/plus.png',    // 自定义增加按钮图标
                    onChange: (value: number) => {
                        this.value11 = value;
                    }
                })
            }
            .width('100%')
            .justifyContent(FlexAlign.SpaceBetween)
            .alignItems(VerticalAlign.Center)
            .padding(10)
        }
        .width('100%')
        .padding(16)
    }
}
